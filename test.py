import argparse
from pathlib import Path
import torch

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "data_path",
        type=Path,
        help="Path to subsampled data",
    )

    parser.add_argument(
        "mask_type",
        choices=("random", "equispaced", "converge"),
        default="random",
        type=str,
        help="Type of k-space mask",
    )
    parser.add_argument(
        "model",
        choices=("Unet", "VarNet", "FVarNet"),
        type=str,
        help="Type of model",
    )
    parser.add_argument(
        "accelerations",
        nargs="+",
        default=[4],
        type=int,
        help="Acceleration rates to use for masks",
    )
    parser.add_argument(
        "output_path",
        type=Path,
        help="Path to output dir",
    )

    args = parser.parse_args()
    args.center_fractions = [0.08] if args.accelerations == [4] else [0.04]
    if args.model == 'Unet':
        args.state_dict_file = 'knee_unet_state_dict.pt'
    elif args.model == 'VarNet':
        args.state_dict_file = 'knee_vn_state_dict.pt'
    else:
        args.state_dict_file = 'knee_fvn_state_dict.pt'
    args.device = 'cpu'
    if args.model == 'Unet':
        from scripts.unet.run_inference import run_inference
        args.challenge = 'unet_knee_mc'
        run_inference(args.challenge,
        args.state_dict_file,
        args.data_path,
        args.output_path,
        torch.device(args.device))
    elif args.model == 'VarNet':
        from scripts.varnet.run_inference import run_inference
        args.challenge = 'varnet_knee_mc'
        run_inference(args.challenge,
                      args.state_dict_file,
                      args.data_path,
                      args.output_path,
                      torch.device(args.device),args)
    elif args.model == 'FVarNet':
        from scripts.fvarnet.run_interfance import run_inference
        args.challenge = 'fvarnet_knee_mc'
        run_inference(args.challenge,
                      args.state_dict_file,
                      args.data_path,
                      args.output_path,
                      torch.device(args.device),args)
    else:
        raise ValueError(f'Wrong model name {args.model}')
