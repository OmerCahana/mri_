import argparse
from pathlib import Path
from code.pl_modules import FastMriDataModule
import pathlib
from code.data.mri_data import fetch_dir


if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    # client arguments
    parser.add_argument(
        "data_path",
        type=Path,
        help="Path to subsampled data",
    )

    parser.add_argument(
        "model",
        choices=("Unet", "VarNet", "FVarNet"),
        type=str,
        help="Type of model",
    )

    parser.add_argument(
        "accelerations",
        nargs="+",
        default=[4],
        type=int,
        help="Acceleration rates to use for masks",
    )

    # data transform params
    parser.add_argument(
        "mask_type",
        choices=("random", "equispaced", "converge"),
        default="random",
        type=str,
        help="Type of k-space mask",
    )

    parser.add_argument(
        "scan_type",
        choices=("brain", "knee"),
        default="brain",
        type=str,
        help="Type of scan {brain,knee}",
    )
    parser.add_argument(
        "output_path",
        type=Path,
        help="Path to output dir",
    )
    parser = FastMriDataModule.add_data_specific_args(parser)
    parser.set_defaults(
        in_chans=1,  # number of input channels to U-Net
        out_chans=1,  # number of output chanenls to U-Net
        chans=32,  # number of top-level U-Net channels
        num_pool_layers=4,  # number of U-Net pooling layers
        drop_prob=0.0,  # dropout probability
        lr=0.001,  # RMSProp learning rate
        lr_step_size=40,  # epoch at which to decrease learning rate
        lr_gamma=0.1,  # extent to which to decrease learning rate
        weight_decay=0.0,  # weight decay regularization strength
    )
    num_gpus = 1
    backend = "ddp"
    batch_size = 1 if backend == "ddp" else num_gpus
    path_config = pathlib.Path("../../fastmri_dirs.yaml")
    default_root_dir = fetch_dir("log_path", path_config) / "unet" / "unet_demo"

    parser.set_defaults(
        gpus=num_gpus,  # number of gpus to use
        replace_sampler_ddp=False,  # this is necessary for volume dispatch during val
        accelerator=backend,  # what distributed version to use
        seed=42,  # random seed
        deterministic=True,  # makes things slower, but deterministic
        default_root_dir=default_root_dir,  # directory for logs and checkpoints
        max_epochs=50,  # max number of epochs
    )
    args = parser.parse_args()
    args.mode = 'train'
    args.seed = 42
    args.accelerator = 'ddp'
    args.center_fractions = [0.08] if args.accelerations == [4] else [0.04]
    args.challenge = 'multicoil'
    args.test_split = 'test'
    args.num_gpus = 1


    if args.model == 'Unet':
        from scripts.unet.train_unet import cli_main
        cli_main(args)
    elif args.model == 'VarNet':
        from scripts.varnet.train_varnet import cli_main

        parser.set_defaults(
            num_cascades=8,  # number of unrolled iterations
            pools=4,  # number of pooling layers for U-Net
            chans=18,  # number of top-level channels for U-Net
            sens_pools=4,  # number of pooling layers for sense est. U-Net
            sens_chans=8,  # number of top-level channels for sense est. U-Net
            lr=0.001,  # Adam learning rate
            lr_step_size=40,  # epoch at which to decrease learning rate
            lr_gamma=0.1,  # extent to which to decrease learning rate
            weight_decay=0.0,  # weight regularization strength
        )
        num_gpus = 1
        backend = "ddp"
        batch_size = 1 if backend == "ddp" else num_gpus
        path_config = pathlib.Path("../../fastmri_dirs.yaml")
        default_root_dir = fetch_dir("log_path", path_config) / "unet" / "unet_demo"

        parser.set_defaults(
            gpus=num_gpus,  # number of gpus to use
            replace_sampler_ddp=False,  # this is necessary for volume dispatch during val
            accelerator=backend,  # what distributed version to use
            seed=42,  # random seed
            deterministic=True,  # makes things slower, but deterministic
            default_root_dir=default_root_dir,  # directory for logs and checkpoints
            max_epochs=50,  # max number of epochs
        )
        args = parser.parse_args()
        args.mode = 'train'
        args.accelerator = 'ddp'
        args.center_fractions = [0.08] if args.accelerations == [4] else [0.04]
        args.challenge = 'multicoil'
        args.test_split = 'test'
        cli_main(args)
    elif  args.model == 'FVarNet':
        from scripts.fvarnet.train_fvarnet import cli_main

        parser.set_defaults(
            num_cascades=8,  # number of unrolled iterations
            pools=4,  # number of pooling layers for U-Net
            chans=18,  # number of top-level channels for U-Net
            sens_pools=4,  # number of pooling layers for sense est. U-Net
            sens_chans=8,  # number of top-level channels for sense est. U-Net
            lr=0.001,  # Adam learning rate
            lr_step_size=40,  # epoch at which to decrease learning rate
            lr_gamma=0.1,  # extent to which to decrease learning rate
            weight_decay=0.0,  # weight regularization strength
        )
        num_gpus = 1
        backend = "ddp"
        batch_size = 1 if backend == "ddp" else num_gpus
        path_config = pathlib.Path("../../fastmri_dirs.yaml")
        default_root_dir = fetch_dir("log_path", path_config) / "unet" / "unet_demo"

        parser.set_defaults(
            gpus=num_gpus,  # number of gpus to use
            replace_sampler_ddp=False,  # this is necessary for volume dispatch during val
            accelerator=backend,  # what distributed version to use
            seed=42,  # random seed
            deterministic=True,  # makes things slower, but deterministic
            default_root_dir=default_root_dir,  # directory for logs and checkpoints
            max_epochs=50,  # max number of epochs
        )
        args = parser.parse_args()
        args.mode = 'train'
        args.accelerator = 'ddp'
        args.center_fractions = [0.08] if args.accelerations == [4] else [0.04]
        args.challenge = 'multicoil'
        args.test_split = 'test'
        cli_main(args)
    else:
        raise ValueError(f'Wrong model name {args.model}')
